./gradlew clean jar
mvn deploy:deploy-file \
	-DgroupId=io.spinnaker.embedded-redis \
	-DartifactId=embedded-redis \
	-Dversion=0.7.1 \
	-Dpackaging=jar \
	-Dfile=./embedded-redis-0.7.1.jar \
	-Durl=http://192.168.1.27:8082/nexus/content/repositories/releases \
	-DrepositoryId=nexus-releases